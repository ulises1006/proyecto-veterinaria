Rails.application.routes.draw do

  devise_for :users
  devise_for :admins, controllers: { sessions: 'admins/sessions' } 
  devise_for :usuarios, controllers: { sessions: 'usuarios/sessions' }
  
  resources :pets do
  resources :appointments
 end
 root to: 'pets#index'

 devise_scope :admin do
	get 'admins/users/list', to: 'admins/sessions#list', as: 'users_list'
end

 get'/citas', to: 'appointments#index', as: 'appointments'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
