class Pet < ApplicationRecord
	has_many :appointments
	belongs_to :usuario
	mount_uploader :avatar, FotoUploader
	validates :nombre, presence: true
	validates :edad, numericality: {only_integer: true, message: "Solo numeros enteros"}
	validates :estado, presence: true
end
