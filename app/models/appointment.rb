class Appointment < ApplicationRecord
	belongs_to :pet
	validates :fecha, presence: true
	validates :hora, presence: true
	validates :consultorio, presence: true
end
