json.extract! appointment, :id, :fecha, :hora, :consultorio, :pet_id, :created_at, :updated_at
json.url appointment_url(appointment, format: :json)
