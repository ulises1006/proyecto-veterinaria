class RemoveEstadoFromPets < ActiveRecord::Migration[5.0]
  def change
    remove_column :pets, :estado, :boolean
  end
end
