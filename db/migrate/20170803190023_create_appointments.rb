class CreateAppointments < ActiveRecord::Migration[5.0]
  def change
    create_table :appointments do |t|
      t.date :fecha
      t.time :hora
      t.string :consultorio
      t.integer :pet_id

      t.timestamps
    end
  end
end
