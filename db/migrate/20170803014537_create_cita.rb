class CreateCita < ActiveRecord::Migration[5.0]
  def change
    create_table :cita do |t|
      t.date :fecha
      t.time :hora
      t.integer :mascota_id

      t.timestamps
    end
  end
end
