class AddEstadoToPets < ActiveRecord::Migration[5.0]
  def change
    add_column :pets, :estado, :string
  end
end
