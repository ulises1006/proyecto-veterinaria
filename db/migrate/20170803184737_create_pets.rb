class CreatePets < ActiveRecord::Migration[5.0]
  def change
    create_table :pets do |t|
      t.string :nombre
      t.string :especie
      t.integer :edad
      t.boolean :estado
      t.string :avatar

      t.timestamps
    end
  end
end
