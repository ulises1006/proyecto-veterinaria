class CreateMascota < ActiveRecord::Migration[5.0]
  def change
    create_table :mascota do |t|
      t.string :nombre
      t.string :especie
      t.integer :edad
      t.string :estado
      t.string :avatar

      t.timestamps
    end
  end
end
